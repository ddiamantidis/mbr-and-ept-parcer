__author__ = 'Dimitrios'

import unittest
import io
import contextlib
from ept_parcer import help_file


class TestingHelpFile(unittest.TestCase):
    def test_help_file_function(self):
        output_captured = io.StringIO()
        with contextlib.redirect_stdout(output_captured):
            help_file()
            function_output = output_captured.getvalue()
        self.assertEqual(type(function_output), str)   # Tests if help_file() function returns a string
        self.assertEqual(len(function_output), 271)    # Tests if help_file() function output length is correct


if __name__ == '__main__':
    unittest.main()
