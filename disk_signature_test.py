__author__ = 'Dimitrios'

import unittest
import io
import contextlib
from ept_parcer import disk_signature_information


class TestingHelpFile(unittest.TestCase):
    def test_disk_signature_function(self):
        output_captured = io.StringIO()
        with contextlib.redirect_stdout(output_captured):
            disk_signature_information(open('test.001', 'rb'))
            function_output = output_captured.getvalue()
        self.assertEqual(type(function_output), str)  # Tests if  function returns a string
        self.assertEqual(len(function_output), 40)    # Tests if  function output length is correct


if __name__ == '__main__':
    unittest.main()
