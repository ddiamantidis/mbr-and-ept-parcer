# README #


###    How to use this Script    ###

Script only accepts raw image files (.dd extension)

python ept_parcer.py [-option = argument ] 

or ept_parcer.py [-option = argument ] if .py files are associated with python

Options:

-v --version               Returns Script Version

-h --help                  Returns this Help File

-i --input=file            Selects File to Parce


### Master Boot Record and Extended Partition Table Parcer ###
###                              Version 1.3.2                              ###

This script provides detailed information about partitions found in MBR and EPT tables.

Details include : 

CHS information, LBA information (starting partition sector and size), 

Disk Signature, Partition type ( FAT32, NTFS, OSX, Linux, etc) and Boot status of partition.

When run, the script apart from standard console output, provides a detailed log of the timeline of events that happened

during parcing in order for the viewer to determine what problem, if any, occured.


### Testing ###

Test files do not include the .dd files used due to their size.

Any raw image file will do as long as the name used in test code is edited.


### Contact Details ###

e-mail : d.diamantidis@cranfield.ac.uk