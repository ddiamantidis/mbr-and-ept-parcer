__author__ = 'Dimitrios'

import struct
import getopt
import sys
import logging


# Help file shown on console when user uses option -h
def help_file():
    print('\n     How to use this Script:\n'
    'python ept_parcer.py [-option = argument ]\n\n'
    'Options:\n'
    '-v --version               Returns Script Version\n'
    '-h --help                  Returns this Help File\n'
    '-i --input=file            Selects File to Parce\n'
    'Script only accepts raw image files!!!')


# Provides Console Menu for the Script
def console_options():
    input_file = None
    try:
        opts, args = getopt.getopt(sys.argv[1:], "vhi:", ["version", "help", "input="])
    except getopt.GetoptError as err:  # Wrong option
        wrong_option = '{}, see help file for more information'.format(err)
        print(wrong_option)
        logging.warning(wrong_option)
        sys.exit()
    for opt, arg in opts:
        if opt in ('-h', '--help'):  # Help option
            help_file()
            logging.info('Printed Help File to Console')
            sys.exit()
        elif opt in ('-i', '--input'):  # Input option
            file_given_by_user = open(arg, 'rb')
            logging.info('Opened File {}'.format(arg))
            return file_given_by_user
        elif opt in ('-v', '--version'):  # Version option
            print('Version 1.3.2')
            logging.info('Printed Script Version to Console')
            sys.exit()
    if input_file is None:  # No option
        print('No option selected, see help file for more information')
        logging.warning('No file was selected by User')
        sys.exit()


# Returns the 4 Disk Signature Bytes to Console
def disk_signature_information(binary_file_given):
    logging.info('Started processing disk signature')
    binary_file_given.seek(440)
    disk_signature = binary_file_given.read(4)
    disk_signature_bytes = struct.unpack('<4B', disk_signature)
    disk_signature_output = 'disk signature bytes are {} {} {} {} \n'\
        .format(disk_signature_bytes[0], disk_signature_bytes[1], disk_signature_bytes[2], disk_signature_bytes[3])
    print(disk_signature_output)
    logging.info('Finished processing disk signature')


# Checks for partitions and runs other functions for every partition found
def partition_scan(binary_file_given, starting_sector):
    binary_file_given.seek(starting_sector*512+446)
    for partition in range(4):
        partition_bytes = binary_file_given.read(16)
        if partition_bytes != b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00':
            logging.info('Started Partition Boot Status Check')
            partition_boot_status(partition, partition_bytes)
            logging.info('Finished Partition Boot Status Check')
            logging.info('Started processing LBA information')
            lba_information(starting_sector, partition_bytes)
            logging.info('Finished processing LBA information')
            logging.info('Started processing CHS information')
            chs_information(partition_bytes)
            logging.info('Finished processing CHS information')
            logging.info('Started processing partition type information')
            partition_type_information(starting_sector, partition_bytes)
            logging.info('Finished processing partition type information')
        else:  # Will terminate program is a blank entry is found, due to the way MBRs and EPTs are constructed
            logging.info('No other partitions found, Parcer Finished')
            sys.exit()


# Checks boot status of read partition
def partition_boot_status(partition, bytes_read):
    boot_status = struct.unpack('B', bytes_read[0:1])[0]
    if boot_status not in (0, 128):  # Integer values for 0x00 0x80
        print('Partition Boot Byte is invalid, make sure file given has an Master Boot Record\n')
        logging.warning('Partition Boot Byte is invalid')
        sys.exit()
    partition_details_label = 'Partition {} Information:'.format(partition+1)
    print(partition_details_label)
    if boot_status is 0:  # Not Bootable partition found
        print('Partition is not bootable\n')
        logging.info('Partition {} is not bootable'.format(partition+1))
    elif boot_status is 128:  # Bootable partition found
        print('Partition is bootable\n')
        logging.info('Partition {} is bootable'.format(partition+1))


# Provides LBA information for the partition read
def lba_information(starting_sector, bytes_read):
    lba_first_sector = starting_sector + struct.unpack('<I', bytes_read[8:12])[0]
    partition_sectors = struct.unpack('i', bytes_read[12:16])[0]
    lba_info_output = 'First LBA sector = {}\n''Size in sectors = {}\n'.format(lba_first_sector, partition_sectors)
    print(lba_info_output)


# Provides CHS information of the partition read
def chs_information(bytes_read):
    first_head = struct.unpack('B', bytes_read[1:2])[0]
    first_sector = struct.unpack('B', bytes_read[2:3])[0]
    first_cylinder = struct.unpack('B', bytes_read[3:4])[0]
    last_head = struct.unpack('B', bytes_read[5:6])[0]
    last_sector = struct.unpack('B', bytes_read[6:7])[0]
    last_cylinder = struct.unpack('B', bytes_read[7:8])[0]
    chs_info_output = 'CHS Information:\n''First Head = {}\n''First Sector = {}\n''First Cylinder = ' \
                      '{}\n''Last Head = {}\n''Last Sector = {}\n''Last Cylinder = {}'\
        .format(first_head, first_sector, first_cylinder, last_head, last_sector, last_cylinder)
    print(chs_info_output)


# Provides Partition type information based on the dictionary provided
def partition_type_information(starting_sector, bytes_read):
    partition_type = struct.unpack('B', bytes_read[4:5])[0]
    partition_type_table = {
    0x00: 'Empty', 0x1: 'FAT32', 0x4: 'FAT16 up to 32MB', 0x6: 'FAT32 greater than 32MB', 0x7: 'NTFS/ExFAT',
    0xB: 'FAT32', 0xC: 'FAT32 LBA mapped', 0xE: 'FAT32 greater than 32MB LBA mapped', 0x82: 'Linux Swap Partition',
    0x83: 'Linux Partition', 0xAF: 'Mac OS HFS / HFS+ Intel Partition', 0x5: 'Extended Partition',
    0xF: 'Extended Partition LBA mapped', 0x85: 'Linux Extended Partition'
    }
    partition_type_name = partition_type_table[partition_type]
    partition_type_output = '\nPartition type is {}, byte value {}\n'.format(partition_type_name, hex(partition_type))
    print(partition_type_output)
    if partition_type in (0x5, 0xF, 0x85):
        logging.info('Extended partition found, started processing EPT information')
        extended_partition_check(starting_sector, bytes_read)


# Checks for Extended Partitions and runs partition_scan function on their EPT
def extended_partition_check(starting_sector, bytes_read):
    extended_partition_first_sector = starting_sector + struct.unpack('<I', bytes_read[8:12])[0]
    partition_scan(file_pointer, extended_partition_first_sector)  # This will provide information for EPT


if __name__ == "__main__":
    logging.basicConfig(filename='master_boot_record_parcer.log', filemode='w',
                        format='%(levelname)s:%(asctime)s %(message)s', level=logging.INFO)
    logging.info('EPT Parcer started')
    file_pointer = console_options()
    disk_signature_information(file_pointer)
    logging.info('Started processing MBR partition information')
    partition_scan(file_pointer, 0)  # This will provide information for the MBR Partitions